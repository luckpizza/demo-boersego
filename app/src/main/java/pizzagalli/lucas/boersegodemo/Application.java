package pizzagalli.lucas.boersegodemo;

import pizzagalli.lucas.boersegodemo.utils.Network;

public class Application extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Network.init();
    }
}
