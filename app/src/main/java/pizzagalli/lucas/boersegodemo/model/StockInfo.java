package pizzagalli.lucas.boersegodemo.model;

public class StockInfo {
    
    private String name;
    private String price;
    private int id;

    private StockInfo() {

    }
    public StockInfo(int id, String name) {
        this.name = name;
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public String getImageUrl() {
        return "http://charting.godmode-trader.de/t/gmt/sparkline?iid=" + id +"&width=60&height=60";
    }

    public void setPrice(String price) {
        this.price = price;
    }

/*    public static StockInfo getFakeStock() {
        StockInfo stockInfo = new StockInfo();
        stockInfo.name = "Stock Name " + count;
        stockInfo.price = "" + count;
        stockInfo.imageUrl = "http://files.softicons.com/download/game-icons/super-mario-icons-by-sandro-pereira/png/128/Hat%20-%20Luigi.png";
        return stockInfo;
    }*/

}
