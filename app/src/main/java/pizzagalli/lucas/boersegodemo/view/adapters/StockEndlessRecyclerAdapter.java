package pizzagalli.lucas.boersegodemo.view.adapters;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import pizzagalli.lucas.boersegodemo.R;
import pizzagalli.lucas.boersegodemo.model.StockInfo;
import pizzagalli.lucas.boersegodemo.utils.Network;

public class StockEndlessRecyclerAdapter extends RecyclerView.Adapter {

    private static final String TAG = "StockEndlessRecyclerAdapter";
    private LoadMoreDataCallback mLoadMoreDataCallback;
    private LayoutInflater mLayoutInflater;
    private List<StockInfo> mList;

    public StockEndlessRecyclerAdapter(Activity context, LoadMoreDataCallback loadMoreDataCallback) {
        mLoadMoreDataCallback = loadMoreDataCallback;
        mLayoutInflater = context.getLayoutInflater();
        mList = new ArrayList<>();
        mLoadMoreDataCallback.loadMoreData(0);
        final List<StockInfo> newStocks = new ArrayList<>();
        addData(newStocks);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mLayoutInflater.inflate(R.layout.layout_stock_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final StockInfo stockInfo = mList.get(position);
        final ViewHolder myHolder = (ViewHolder) holder;
        myHolder.priceTextView.setText(stockInfo.getPrice());
        myHolder.stockNameTextView.setText(stockInfo.getName());
        myHolder.imageUrl = stockInfo.getImageUrl();
        myHolder.imageView.setImageBitmap(null);
        if (mList.size() - 2 == position) {
            mLoadMoreDataCallback.loadMoreData(mList.size());
        }
        Network.getBitmapFromURL(stockInfo.getImageUrl(), new Network.NetworkRequestCallback<Bitmap>() {
            @Override
            public void onSuccess(String requestUrl, Bitmap response) {
                if (requestUrl.equals(myHolder.imageUrl)) {
                    myHolder.imageView.setImageBitmap(response);
                } else {
                    Log.d(TAG, "Image downloaded from old view");
                }
            }

            @Override
            public void onError(String requestUrl, Exception e) {
                Log.d(TAG, "Error downloading image with exception:" + e);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    private static class ViewHolder extends RecyclerView.ViewHolder {

        TextView stockNameTextView;
        TextView priceTextView;
        ImageView imageView;
        String imageUrl;

        public ViewHolder(View itemView) {
            super(itemView);
            stockNameTextView = (TextView) itemView.findViewById(R.id.name);
            priceTextView = (TextView) itemView.findViewById(R.id.price);
            imageView = (ImageView) itemView.findViewById(R.id.image);

        }
    }

    public void addData(List<StockInfo> list) {
        int originalSize = mList.size();
        filterDupplicated(list);
        mList.addAll(list);
        notifyItemRangeInserted(originalSize, mList.size());
    }

    private void filterDupplicated(List<StockInfo> list) {
        Iterator<StockInfo> iterator = list.iterator();
        while (iterator.hasNext()) {
            if(mList.contains(iterator.next())) {
                iterator.remove();
            }
        }
    }


    public interface LoadMoreDataCallback {
        /**
         * Implement in order to feed the adapter with more data
         *
         * @param forPosition last data position the adapter has
         */
        void loadMoreData(int forPosition);
    }


}
