package pizzagalli.lucas.boersegodemo.view;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import pizzagalli.lucas.boersegodemo.R;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

}
