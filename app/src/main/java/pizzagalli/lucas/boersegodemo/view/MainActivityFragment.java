package pizzagalli.lucas.boersegodemo.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import pizzagalli.lucas.boersegodemo.R;
import pizzagalli.lucas.boersegodemo.model.StockInfo;
import pizzagalli.lucas.boersegodemo.utils.Network;
import pizzagalli.lucas.boersegodemo.view.adapters.StockEndlessRecyclerAdapter;

public class MainActivityFragment extends Fragment {

    private static final String TAG = "MainActivityFragment";
    private RecyclerView mRecyclerView;
    private StockEndlessRecyclerAdapter mAdapter;
    private Network.NetworkTask mCurrentTask;


    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_stock_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        mAdapter = new StockEndlessRecyclerAdapter(getActivity(), new StockEndlessRecyclerAdapter.LoadMoreDataCallback() {
            @Override
            public void loadMoreData(int forPosition) {
                if (mCurrentTask == null || !mCurrentTask.isOnGoing) {
                    mCurrentTask = Network.loadStockInfo(forPosition, new Network.NetworkRequestCallback<List<StockInfo>>() {
                        @Override
                        public void onSuccess(String requestUrl, List<StockInfo> response) {
                            mAdapter.addData(response);
                        }

                        @Override
                        public void onError(String requestUrl, Exception e) {

                            Log.e(TAG, "Error loading url:" + requestUrl);
                        }
                    });
                }
            }
        });
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL, false));
    }
}
