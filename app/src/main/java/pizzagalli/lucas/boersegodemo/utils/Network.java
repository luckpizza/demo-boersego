package pizzagalli.lucas.boersegodemo.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


import pizzagalli.lucas.boersegodemo.model.StockInfo;

public class Network {

    private static final int STOCK_PER_PAGE = 10;
    private static final String TAG = "Network";
    private static LooperThread sWorkingThread =   new LooperThread();
    private static LooperThread sImageThread =   new LooperThread();

    private static Handler sWorkingHandler;
    private static Handler sImageHandler;

    private static Handler sUiHandler = new Handler(Looper.getMainLooper());

    private static String sAuthToken;
    private static String EP_STOCK_LIST;

    public static void init() {
        //I am blocking the UI in order to initialize the stuff I need,
        // this can be dangerous but for a demo seems ok.
        sWorkingHandler= initHandler(sWorkingThread);
        sImageHandler= initHandler(sImageThread);
        getAuthorizationToken();
    }

    private static void getAuthorizationToken() {
        NetworkTask task = get("https://auth.boerse-go.de/session", new NetworkRequestCallback<String>() {

            @Override
            public void onSuccess(String requestUrl, String response) {
                sAuthToken = response;
                EP_STOCK_LIST = "http://api.boerse-go.de/v1/instrument/list.json?do=list&filter=componentOf.id=133955&attributes=refQuotation[quote,currency]&_t=" + sAuthToken;
            }

            @Override
            public void onError(String requestUrl, Exception e) {

            }
        }, new AuthTokenParser(), RequestPriority.HIGH, sWorkingHandler);

        while(EP_STOCK_LIST == null && task.isOnGoing ) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static Handler initHandler(LooperThread thread) {
        thread.start();
        while (thread.mHandler == null) {
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return thread.mHandler;

    }

    public static NetworkTask getBitmapFromURL(final String link, final NetworkRequestCallback<Bitmap> callback) {
    /*--- this method downloads an Image from the given URL,
     *  then decodes and returns a Bitmap object
     ---*/

        return get(link, callback, new NetworkParser<Bitmap>() {
            @Override
            public Bitmap parse(InputStream input) {
                return BitmapFactory.decodeStream(input);
            }
        }, RequestPriority.LOW, null);
    }

    public static  NetworkTask loadStockInfo(int forPosition, final NetworkRequestCallback<List<StockInfo>> networkRequestCallback) {
        return get(EP_STOCK_LIST + "&limit=" + STOCK_PER_PAGE + "&page=" + ((forPosition / STOCK_PER_PAGE) + 1)
                , networkRequestCallback, new StockInfoParser(), RequestPriority.HIGH, null);
    }

    public static <T> NetworkTask get(final String link, final NetworkRequestCallback<T> networkRequestCallback
            , final NetworkParser<T> parser, RequestPriority priority, Handler callBackHandler) {
        Log.d(TAG, "NetworkRequest to: " + link);
        final  Handler handler = callBackHandler == null ? sUiHandler : callBackHandler;
        NetworkTask task = new NetworkTask() {
            @Override
            public void run() {
                try {
                    URL url = new URL(link);
                    HttpURLConnection connection = (HttpURLConnection) url
                            .openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    final T response = parser.parse(input);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            networkRequestCallback.onSuccess(link, response);
                            isOnGoing = false;
                        }
                    });

                } catch (final Exception e) {
                    e.printStackTrace();
                    Log.e(TAG,"Error getting url:" + e.getMessage().toString());
                    handler.post(new Runnable() {

                        @Override
                        public void run() {
                            networkRequestCallback.onError(link, e);
                            isOnGoing = false;
                        }
                    });
                }
            }
        };
        if (priority == RequestPriority.HIGH) {
            sWorkingHandler.post(task);
        } else if (priority == RequestPriority.LOW) {
            sImageHandler.post(task);
        } else {
            throw new IllegalArgumentException("Request priority not supported");
        }
        return task;
    }

    private static JSONObject getJsonObject(InputStream input) throws IOException, JSONException {
        BufferedReader streamReader = new BufferedReader(new InputStreamReader(input, "UTF-8"));
        StringBuilder responseStrBuilder = new StringBuilder();

        String inputStr;
        while ((inputStr = streamReader.readLine()) != null)
            responseStrBuilder.append(inputStr);
        return  new JSONObject(responseStrBuilder.toString());
    }

    public static class NetworkTask implements Runnable {

        public Boolean isOnGoing = true;
        @Override
        public void run() {

        }
    }

    public interface  NetworkParser<T> {
        T parse(InputStream input);
    }

    public interface  NetworkRequestCallback<T>{
        void onSuccess(String requestUrl, T response);

        void onError(String requestUrl, Exception e);
    }

    private static class AuthTokenParser implements NetworkParser<String> {

        @Override
        public String parse(InputStream input) {
            try {
                JSONObject jsonObject = getJsonObject(input);
                return jsonObject.getJSONObject("tokens").keys().next();

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
    private static class StockInfoParser implements NetworkParser<List<StockInfo>> {

        private static final String TAG = "StockInfoParser";

        @Override
        public List<StockInfo> parse(InputStream input) {
            JSONObject rootJson;
            List<StockInfo> stockInfoList = new ArrayList<>();
            try {
                rootJson = getJsonObject(input);
                JSONArray jsonArray = rootJson.getJSONArray("data");
                for( int i = 0 ; i < jsonArray.length() ; ++i) {
                    JSONObject jsonItem = jsonArray.getJSONObject(i);

                    StockInfo stockInfo =  new StockInfo(jsonItem.getInt("id"), jsonItem.getString("name"));
                    String price = jsonItem.getJSONObject("refQuotation").getJSONObject("currency").getString("symbol")
                            +jsonItem.getJSONObject("refQuotation").getJSONObject("quote").getDouble("value");
                    stockInfo.setPrice(price);
                    stockInfoList.add(stockInfo);
                }

            } catch (Exception e) {
                Log.e(TAG, "Error parsing response with exception: " + e);
            }
            return stockInfoList;
        }
    }



    public enum RequestPriority {
        LOW, HIGH
    }
}
